## The app architecture
The app is based on MVVM architecture, along data binding. 

Following is the description of application structure.

#### The View layer (com.le.cakeinfo.feature)
The View layer comprises of UI (Activity/Fragments) and the viewmodels. The view layer is structured
per feature names, such as feature/main , feature/cakelist

#### The Model a.k.a Data layer (com.le.cakeinfo.datasource)
The data layer handles network operations. The data layer exposes itself via the repository - see com.le.cakeinfo.repository

#### Dependency Injection (com.challenge.poqtest.di)
Main dependencies are configured at com.le.cakeinfo.di, while feature and module specific dependencies are configured
in their respective packages

## Libraries used by the app

##### Dagger2
Used for dependency Injection

##### Retrofit2
Used for communicating and fetching data from the network

##### Retrofit2 to Rx2 Adapter (com.squareup.retrofit2:adapter-rxjava2:x.x.x)
Used for receiving Retrofit2's serialized response objects wrapped in Rx2

##### Lifecycle dependency
Used for ViewModel and LiveData

##### Glide
Used to image caching & loading

##### Junit & Mockito
Used for mocking & unit testing


(Developed with Android Studio 3.4)