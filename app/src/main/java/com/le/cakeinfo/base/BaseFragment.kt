package com.le.cakeinfo.base

import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {

    val disposables = CompositeDisposable()

    override fun onDetach() {
        super.onDetach()
        disposables.clear()
    }
}