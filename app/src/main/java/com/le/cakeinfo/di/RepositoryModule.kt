package com.le.cakeinfo.di

import com.le.cakeinfo.repository.CakesRepository
import com.le.cakeinfo.repository.CakesRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindCakesRepository(cakesRepositoryImpl: CakesRepositoryImpl): CakesRepository
}