package com.le.cakeinfo.di

import com.le.cakeinfo.di.custom.ActivityScope
import com.le.cakeinfo.feature.main.di.MainActivityModule
import com.le.cakeinfo.feature.main.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}