package com.le.cakeinfo.di.custom

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class FragmentModule<T : Fragment> {

    @Binds
    @FragmentScope
    protected abstract fun provideFragment(t: T): Fragment
}