package com.le.cakeinfo.di.custom

import android.app.Activity
import dagger.Binds
import dagger.Module

@Module
abstract class ActivityModule<T : Activity> {

    @Binds
    @ActivityScope
    protected abstract fun provideActivity(t: T): Activity
}