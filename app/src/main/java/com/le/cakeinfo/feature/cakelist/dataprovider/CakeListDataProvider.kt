package com.le.cakeinfo.feature.cakelist.dataprovider

import com.le.cakeinfo.repository.CakesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

// TODO: better implement an interface
class CakeListDataProvider @Inject constructor(
    private val cakesRepository: CakesRepository
) {

    fun getCakeList() =
        cakesRepository.getCakeList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

}