package com.le.cakeinfo.feature.cakelist.vm

import androidx.lifecycle.MutableLiveData
import com.le.cakeinfo.base.BaseViewModel
import com.le.cakeinfo.extensions.SingleLiveData
import com.le.cakeinfo.feature.cakelist.dataprovider.CakeListDataProvider
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class CakeListViewModel @Inject constructor(
    private val dataProvider: CakeListDataProvider
) : BaseViewModel() {

    /*
     * Uses BehaviorSubject and cakeListLoaded flag to avoid fetching list from network when orientation changes.
     * However, best way to achieve caching is via Repository - using either Room or a third party solution, such as Store lib.
     */

    // emits last item when an observer subscribes - useful when fragment re-creates on rotation
    var cakeItems = BehaviorSubject.create<List<CakeListItemViewModel>>()
    val isLoading = MutableLiveData<Boolean>()
    val onError = SingleLiveData<Boolean>()
    val showCakeItemDialog = SingleLiveData<CakeListItemViewModel>()
    private var cakeListLoaded = false

    fun loadCakeItems() {
        if (!cakeListLoaded) {
            load()
        }
    }

    fun reloadCakeItems() = load()

    fun itemClicked(item: CakeListItemViewModel) =
        showCakeItemDialog.apply {
            value = item
        }

    private fun load() {
        isLoading.value = true
        dataProvider.getCakeList().map { cakeList ->
            cakeList.distinct().sortedBy {
                it.title
            }.map {
                CakeListItemViewModel(it.title, it.desc, it.image)
            }
        }.doAfterTerminate {
            isLoading.value = false
        }.doOnSuccess {
            cakeListLoaded = true
        }.doOnError {
            cakeListLoaded = false
        }.subscribe({
            cakeItems.onNext(it)
        }, {
            // TODO: better notify the right error reason
            onError.value = true
        }).addTo(disposables)
    }
}