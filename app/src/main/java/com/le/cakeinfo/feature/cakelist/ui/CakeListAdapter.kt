package com.le.cakeinfo.feature.cakelist.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.le.cakeinfo.databinding.ItemCakeListBinding
import com.le.cakeinfo.feature.cakelist.vm.CakeListItemViewModel

class CakeListAdapter(
    val clickCallback: ((CakeListItemViewModel) -> Unit)
) : RecyclerView.Adapter<CakeListAdapter.CakeListViewHolder>() {

    var items: List<CakeListItemViewModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CakeListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCakeListBinding.inflate(inflater)
        return CakeListViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CakeListViewHolder, position: Int) = holder.bind(items[position])

    inner class CakeListViewHolder(private val binding: ItemCakeListBinding) :
        ViewHolder(binding.root) {

        fun bind(item: CakeListItemViewModel) = binding.let {
            it.root.setOnClickListener {
                clickCallback.invoke(item)
            }
            it.vm = item
            it.executePendingBindings()
        }

    }

}