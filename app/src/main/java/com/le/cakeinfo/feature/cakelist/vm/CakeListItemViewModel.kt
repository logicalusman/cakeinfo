package com.le.cakeinfo.feature.cakelist.vm

data class CakeListItemViewModel(val title: String, val desc: String, val image: String)
