package com.le.cakeinfo.feature.cakelist.di

import com.le.cakeinfo.di.custom.FragmentModule
import com.le.cakeinfo.feature.cakelist.ui.CakeListFragment
import dagger.Module

@Module(includes = [CakeListFragmentSubModule::class])
abstract class CakeListFragmentModule : FragmentModule<CakeListFragment>()