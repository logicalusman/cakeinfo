package com.le.cakeinfo.feature.main.di

import com.le.cakeinfo.di.custom.ActivityModule
import com.le.cakeinfo.di.custom.FragmentScope
import com.le.cakeinfo.feature.cakelist.di.CakeListFragmentModule
import com.le.cakeinfo.feature.cakelist.ui.CakeListFragment
import com.le.cakeinfo.feature.main.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule : ActivityModule<MainActivity>() {

    @FragmentScope
    @ContributesAndroidInjector(modules = [CakeListFragmentModule::class])
    internal abstract fun contributeRepositoryListFragment(): CakeListFragment


}