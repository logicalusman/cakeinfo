package com.le.cakeinfo.feature.cakelist.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.le.cakeinfo.R
import com.le.cakeinfo.base.BaseFragment
import com.le.cakeinfo.databinding.FragmentCakeListBinding
import com.le.cakeinfo.di.custom.ViewModelFactory
import com.le.cakeinfo.feature.cakelist.vm.CakeListViewModel
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject


class CakeListFragment : BaseFragment(), HasSupportFragmentInjector {

    @Inject
    internal lateinit var androidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory<CakeListViewModel>

    private var binding: FragmentCakeListBinding? = null
    private var cakeListAdapter: CakeListAdapter? = null

    private val viewModel: CakeListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(CakeListViewModel::class.java)
    }

    override fun supportFragmentInjector() = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCakeListBinding.inflate(inflater, container, false)
        .apply {
            binding = this
            lifecycleOwner = this@CakeListFragment
            vm = viewModel
        }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cakeListAdapter = CakeListAdapter { viewModel.itemClicked(it) }
        binding?.let { bind ->
            bind.cakesRv.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = cakeListAdapter
            }
        }
        viewModel.apply {
            cakeItems.subscribe {
                cakeListAdapter?.items = it
            }.addTo(disposables)
            onError.observe(this@CakeListFragment, Observer {
                Snackbar
                    .make(view, R.string.data_loading_error, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry) {
                        this.loadCakeItems()
                    }
                    .setActionTextColor(ContextCompat.getColor(view.context, R.color.colorAccent))
                    .show()
            })
            showCakeItemDialog.observe(this@CakeListFragment, Observer {
                AlertDialog.Builder(context!!).apply {
                    setTitle(it.title)
                    setMessage(it.desc)
                    setPositiveButton(android.R.string.ok, null)
                    show()
                }
            })
            loadCakeItems()
        }
    }


}
