package com.le.cakeinfo.datasource.net

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.le.cakeinfo.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

const val TIMEOUT_MS = 60 * 1000L
const val CACHE_SIZE = 10 * 1024 * 1024L

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder()
        .create()

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    fun provideCakesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        application: Application
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
            .readTimeout(TIMEOUT_MS, TimeUnit.MILLISECONDS)
            .connectTimeout(TIMEOUT_MS, TimeUnit.MILLISECONDS)
//          uncomment to enable caching
//            .cache(Cache(application.cacheDir, CACHE_SIZE))

        if (BuildConfig.DEBUG) {
            client.addInterceptor(httpLoggingInterceptor)
        }

        return client.build()
    }

    @Provides
    @Singleton
    fun provideCakesRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.CAKE_API)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideCakesApi(retrofit: Retrofit): CakeApi = retrofit.create(CakeApi::class.java)

}