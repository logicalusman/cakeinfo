package com.le.cakeinfo.datasource.net.model

import com.le.cakeinfo.datasource.model.CakeInfo

data class CakeInfoResponse(
    val title: String,
    val desc: String,
    val image: String
)


fun CakeInfoResponse.toCakeInfo() = CakeInfo(title,desc,image)