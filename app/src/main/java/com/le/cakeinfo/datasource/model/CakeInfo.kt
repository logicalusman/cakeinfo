package com.le.cakeinfo.datasource.model

data class CakeInfo(
    val title: String,
    val desc: String,
    val image: String
)