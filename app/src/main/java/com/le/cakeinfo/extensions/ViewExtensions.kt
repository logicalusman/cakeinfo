package com.le.cakeinfo.extensions

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

@BindingAdapter("divider")
fun RecyclerView.setDivider(isVertical: Boolean = true) =
    addItemDecoration(
        DividerItemDecoration(
            context,
            if (isVertical) DividerItemDecoration.VERTICAL else DividerItemDecoration.HORIZONTAL
        )
    )

@BindingAdapter("image")
fun ImageView.setImage(imagePath: String) =
    Glide.with(context).load(imagePath).into(this)
