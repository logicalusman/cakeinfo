package com.le.cakeinfo.repository

import com.le.cakeinfo.datasource.model.CakeInfo
import io.reactivex.Single

interface CakesRepository {

    fun getCakeList(): Single<List<CakeInfo>>

}