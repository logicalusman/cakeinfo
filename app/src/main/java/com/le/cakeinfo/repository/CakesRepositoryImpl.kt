package com.le.cakeinfo.repository

import com.le.cakeinfo.datasource.model.CakeInfo
import com.le.cakeinfo.datasource.net.CakeApi
import com.le.cakeinfo.datasource.net.model.toCakeInfo
import io.reactivex.Single
import javax.inject.Inject

class CakesRepositoryImpl @Inject constructor(
    private val cakeApi: CakeApi
) : CakesRepository {

    override fun getCakeList(): Single<List<CakeInfo>> = cakeApi.getCakeList().map { cakeList ->
        cakeList.map { it.toCakeInfo() }
    }
}