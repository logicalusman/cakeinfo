package com.le.cakeinfo.feature.cakelist.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.le.cakeinfo.datasource.model.CakeInfo
import com.le.cakeinfo.feature.cakelist.dataprovider.CakeListDataProvider
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class CakeListViewModelTest {

    // need it for testing livedata
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val cakeListDataProvider: CakeListDataProvider = mock()
    private lateinit var viewModel: CakeListViewModel
    private val cakeList = createFakeCakeList()
    private val expectedTransformedCakeList =
        cakeList.distinct().sortedBy { it.title }.map {
            CakeListItemViewModel(it.title, it.desc, it.image)
        }
    private val cakeListSingle =
        Single.create<List<CakeInfo>> {
            it.onSuccess(cakeList)
        }
    private val cakeListSingleError =
        Single.create<List<CakeInfo>> {
            it.onError(Exception())
        }
    private val mockOnErrorObserver: Observer<Boolean?> = mock()


    @Test
    fun test_loadCakeItems_verify_cake_list_items_loaded_as_expected() {
        doReturn(cakeListSingle).`when`(cakeListDataProvider).getCakeList()
        viewModel = CakeListViewModel(cakeListDataProvider)
        viewModel.cakeItems.subscribe {
            // we compare returned and expected arrays
            Assert.assertArrayEquals(expectedTransformedCakeList.toTypedArray(), it.toTypedArray())
        }
        viewModel.loadCakeItems()
    }

    @Test
    fun test_loadCakeItems_verify_cake_list_loading_fails() {
        doReturn(cakeListSingleError).`when`(cakeListDataProvider).getCakeList()
        viewModel = CakeListViewModel(cakeListDataProvider)
        viewModel.onError.observeForever(mockOnErrorObserver)
        // the initial value of onError is null, since it's never notified
        assertEquals(null, viewModel.onError.value)
        viewModel.loadCakeItems()
        // 1- we check the state with the observer
        verify(mockOnErrorObserver).onChanged(true)
        // 2- we check the value directly, since it's notified true
        assertEquals(true, viewModel.onError.value)
    }

    private fun createFakeCakeList() = mutableListOf<CakeInfo>().apply {
        add(CakeInfo("lemon cheesecake", "Lemon cheese cake", "https://cake.image.url"))
        add(CakeInfo("victoria sponge", "Sponge with jam", "https://cake.image.url"))
        add(CakeInfo("banana cake", "banana cake", "https://cake.image.url"))
        add(CakeInfo("birthday cake", "yearly cake", "https://cake.image.url"))
        add(CakeInfo("victoria sponge", "Sponge with jam", "https://cake.image.url"))
        add(CakeInfo("lemon cheesecake", "Lemon cheese cake", "https://cake.image.url"))
    }
}